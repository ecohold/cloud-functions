from flask import Flask, request, jsonify
from flask_swagger_ui import get_swaggerui_blueprint
from google.cloud import firestore

app = Flask(__name__)

# Define o modelo de dados para as transações
class Transaction:
    def __init__(self, transactionId, accountId, amount):
        self.transactionId = transactionId
        self.accountId = accountId
        self.amount = amount

# Define o modelo de dados para as contas
class Account:
    def __init__(self, accountId, balance):
        self.accountId = accountId
        self.balance = balance

# Cria uma instância do Firestore
db = firestore.Client()

# Define a rota para criar uma transação
@app.route("/accounts/<accountId>/transactions", methods=['POST'])
def create_transaction(accountId):
    try:
        transaction_data = request.json
        transaction = Transaction(**transaction_data)

        # Inicia uma transação com nível de isolamento serializável
        batch = db.batch()

        # Cria a transação no Firestore
        transaction_ref = db.collection('transactions').document()
        batch.set(transaction_ref, transaction_data)

        # Obtem o saldo da conta do Firestore
        account_ref = db.collection('accounts').document(accountId)
        account_snapshot = account_ref.get()
        if not account_snapshot.exists:
            return jsonify({"error": "Conta não encontrada"}), 404
        account_data = account_snapshot.to_dict()

        # Verifica se o saldo da conta é suficiente
        if account_data['balance'] + transaction.amount < 0:
            return jsonify({"error": "Saldo insuficiente"}), 400

        # Atualiza o saldo da conta no Firestore
        account_data['balance'] += transaction.amount
        batch.set(account_ref, account_data)

        batch.commit()

        return jsonify({"message": "Transação criada com sucesso"}), 201

    except Exception as e:
        return jsonify({"error": str(e)}), 500

# Configuração do Swagger
SWAGGER_URL = '/swagger'
API_URL = '/api/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "API Flask"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
