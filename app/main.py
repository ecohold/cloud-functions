# app/main.py

from fastapi import FastAPI, Path
from typing import Optional
from pydantic import BaseModel
from google.cloud import firestore

# Define o modelo de dados para as transações
class Transaction(BaseModel):
    transactionId: str
    accountId: str
    amount: float

# Define o modelo de dados para as contas
class Account(BaseModel):
    accountId: str
    balance: float

# Cria uma instância do Firestore
db = firestore.Client()

# Cria uma instância do FastAPI
app = FastAPI()

# Define a rota para criar uma transação
@app.post("/accounts/{accountId}/transactions")
async def create_transaction(
    accountId: str = Path(..., title="ID da conta"),
    transaction: Transaction = ...,
):
    # Inicia uma transação com nível de isolamento serializável
    with db.batch() as batch:
        try:
            # Cria a transação no Firestore
            batch.set(db.collection('transactions').document(), transaction.dict())

            # Obtem o saldo da conta do Firestore
            account = db.collection('accounts').document(accountId).get().to_dict()

            # Verifica se o saldo da conta é suficiente
            if account['balance'] + transaction.amount < 0:
                raise ValueError('Saldo insuficiente')

            # Atualiza o saldo da conta no Firestore
            account['balance'] += transaction.amount
            batch.set(db.collection('accounts').document(accountId), account)

        except Exception as e:
            # Reverte a transação em caso de erro
            batch.rollback()
            raise e

    return {"message": "Transação criada com sucesso"}
